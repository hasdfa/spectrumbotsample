package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"spectrumBotSample/controller"
	"spectrumBotSample/service/telegramService"
	"spectrumBotSample/tempData"
	"spectrumBotSample/utils"
	"time"
)

var (
	TelegramService telegramService.IService

	signals = make(chan os.Signal, 1)
	running = true
)

func init() {
	flag.Parse()
	for _, i := range tempData.ItemsSource {
		fmt.Print("\rStart loading...")
		tempData.StoreImage(i.Image)
		fmt.Print("\rSaved " + i.Image)
	}
	fmt.Print("\r")
}

func main() {
	TelegramService = telegramService.New(controller.DefaultRouter)
	signal.Notify(signals)
	go waiter()
	go gcrunner()

	if err := TelegramService.StartWebhook(utils.CT_TelegramApiKey); err != nil {
		panic(err)
	}
	TelegramService.Wait()
}

func gcrunner() {
	for running {
		select {
		case <-time.Tick(time.Second * 3):
			runtime.GC()
		}
	}
}

func waiter() {
	defer TelegramService.Stop()
	running = false

	s := <-signals
	fmt.Println("Signal <- " + s.String())
	fmt.Println("Finishing...")
	TelegramService.DeleteAll()
}
