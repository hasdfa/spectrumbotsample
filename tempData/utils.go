package tempData

import (
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
	"sync"
)

var (
	StaticSaver = &sync.Map{}

	tempDirectory = os.TempDir()
)

func GetImage(image string) []byte {
	is := strings.Split(image, "/")
	id := is[len(is)-1]
	bytes, _ := ioutil.ReadFile(path.Join(tempDirectory, id))
	return bytes
}

func StoreImage(image string) {
	is := strings.Split(image, "/")
	id := is[len(is)-1]

	resp, err := http.Get(image)
	if err != nil {
		panic(err)
	}
	file, err := os.Create(path.Join(tempDirectory, id))
	if err != nil {
		panic(err)
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	_, err = file.Write(bytes)
	if err != nil {
		panic(err)
	}
}

func Contains(cart Cart, i *Item) bool {
	_, ok := cart[i.ID]
	return ok
}
