package tempData

import (
	"fmt"
	"strconv"
	"strings"
)

type (
	Item struct {
		ID       string
		Title    string
		Company  string
		Category string
		Image    string

		Price float32
		Sale  byte
	}

	CartItem struct {
		Item  *Item
		Count int
	}

	Cart map[string]*CartItem
)

var (
	source = map[string]string{
		"0": "̶0",
		"1": "̶1",
		"2": "̶2",
		"3": "̶3",
		"4": "̶4",
		"5": "̶5",
		"6": "̶6",
		"7": "̶7",
		"8": "̶8",
		"9": "̶9",
	}
)

func (c Cart) Count() (cn int) {
	for _, i := range c {
		cn += i.Count
	}
	return
}

func (i *Item) ZPrice() (s string) {
	arr := strings.Split(strconv.Itoa(int(i.Price)), "")
	for i := 0; i < len(arr); i++ {
		s += source[arr[i]]
	}
	return
}

func (i *Item) FinalPrice() float32 {
	if i.Sale > 0 {
		return i.Price - i.Price*float32(i.Sale)/100
	}
	return i.Price
}

func (i *Item) FinalPriceString() string {
	return fmt.Sprintf("%.2f", i.FinalPrice())
}

func (i *Item) PriceWithSale() string {
	return fmt.Sprintf("%.0f", i.Price-i.Price*float32(i.Sale)/100)
}

func (i *Item) ImageBytes() []byte {
	return GetImage(i.Image)
}
