package tempData

import "github.com/google/uuid"

const (
	CategoryIPhones   = "📱 iPhone"
	CategoryMacBooks  = "💻 MacBook"
	CategoryIMacs     = "🖥 iMac"
	CategoryEarphones = "🎧 Наушники"
)

var (
	ItemsSource = []*Item{
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xr 64Gb Black (MRY42)",
			Category: CategoryIPhones,
			Price:    22499,
			Image:    "https://i.citrus.ua/imgcache/size_500/uploads/shop/5/5/5533198c217ea9a4280fdf804df8f088.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xr 64Gb Blue (MRY42)",
			Category: CategoryIPhones,
			Price:    22499,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/3/1/31ade1c1719649185f55b228490c53c9.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xr 64Gb Coral (MRY42)",
			Category: CategoryIPhones,
			Price:    22499,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/b/5/b529eabfe43cda6f4ae5e20bed95971c.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xr 64Gb Red (MRY42)",
			Category: CategoryIPhones,
			Price:    22499,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/5/1/51d9561289c71e3d049b086f94526b65.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xr 64Gb White (MRY42)",
			Category: CategoryIPhones,
			Price:    22499,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/7/4/740f192758b204f73ac652c99aa9fe90.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xr 64Gb Yellow (MRY42)",
			Category: CategoryIPhones,
			Price:    22499,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/e/4/e4ed55442a8a6c77340cfb8c30195420.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 64Gb Gold (MT9G2)",
			Category: CategoryIPhones,
			Price:    28999,
			Sale:     9,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/3/c/3ca7aabff2212d386690746e4f259df9.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 64Gb Silver (MT9G2)",
			Category: CategoryIPhones,
			Price:    28999,
			Sale:     9,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/a/9/a96e26131f142be39b6fc791ff9bbe57.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 64Gb Black (MT9G2)",
			Category: CategoryIPhones,
			Price:    28999,
			Sale:     9,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/5/2/52a1af89243681e785220cfd807da6ee.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 256G Gold (MT9G2)",
			Category: CategoryIPhones,
			Price:    33999,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/3/c/3ca7aabff2212d386690746e4f259df9.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 256G Silver (MT9G2)",
			Category: CategoryIPhones,
			Price:    33999,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/a/9/a96e26131f142be39b6fc791ff9bbe57.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 256G Black (MT9G2)",
			Category: CategoryIPhones,
			Price:    33999,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/5/2/52a1af89243681e785220cfd807da6ee.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 512G Gold (MT9G2)",
			Category: CategoryIPhones,
			Price:    42999,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/3/c/3ca7aabff2212d386690746e4f259df9.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 512G Silver (MT9G2)",
			Category: CategoryIPhones,
			Price:    42999,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/a/9/a96e26131f142be39b6fc791ff9bbe57.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iPhone Xs 512G Black (MT9G2)",
			Category: CategoryIPhones,
			Price:    42999,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/5/2/52a1af89243681e785220cfd807da6ee.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Pro Retina 13' 128GB Silver (MPXR2)",
			Category: CategoryMacBooks,
			Price:    41999,
			Sale:     13,
			Image:    "https://i.citrus.ua/imgcache/size_500/uploads/shop/f/f/ff2bd024136fe298904e861beec15792.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Pro Retina 13' 128GB Space Gray (MPXR2)",
			Category: CategoryMacBooks,
			Price:    41999,
			Sale:     13,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/e/e/ee3f784675b911801784fd10d99aecad.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Air 13' 128Gb Gold (MREE2) 2018",
			Category: CategoryMacBooks,
			Price:    40999,
			Sale:     5,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/a/a/aa6f60b4494f3c63b397aca0196176cb.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Air 13' 128Gb Silver (MREE2) 2018",
			Category: CategoryMacBooks,
			Price:    40999,
			Sale:     5,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/8/5/857ab47a4205b57bae68fa351d64e8fb.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Air 13' 128Gb Space gray (MREE2) 2018",
			Category: CategoryMacBooks,
			Price:    40999,
			Sale:     5,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/5/5/5532a6b5e4f2b40a700f2c3c33948d6a.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Pro Touch Bar 15' 256Gb Silver (MR962) 2018",
			Category: CategoryMacBooks,
			Price:    77999,
			Sale:     14,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/d/1/d1c15eb8939eec3bccfe07c4a9acd1cd.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Pro Touch Bar 15' 256Gb Space Gray (MR932) 2018",
			Category: CategoryMacBooks,
			Price:    77999,
			Sale:     14,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/0/5/05a092487c3220c6beaa75fd66f56779.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Pro Touch Bar 15' 256Gb Silver (MR962) 2018",
			Category: CategoryMacBooks,
			Price:    90999,
			Sale:     12,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/d/1/d1c15eb8939eec3bccfe07c4a9acd1cd.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "MacBook Pro Touch Bar 15' 256Gb Space Gray (MR932) 2018",
			Category: CategoryMacBooks,
			Price:    90999,
			Sale:     12,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/0/5/05a092487c3220c6beaa75fd66f56779.jpg",
		},

		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iMac 21,5' (MRT32) 2019",
			Category: CategoryIMacs,
			Price:    47999,
			Sale:     8,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/f/9/f9b4d8c37faedd0a372876fdcc85605f.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iMac with Retina 5K display 27' (MK482UA/A)",
			Category: CategoryIMacs,
			Price:    63999,
			Sale:     8,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/a/9/a93c827a64ab6ef6dc704bb0f5f38411.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iMac with Retina 5K display 27' (MK482UA/A)",
			Category: CategoryIMacs,
			Price:    66999,
			Sale:     9,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/2/f/2ff49759d4735de7410a126973915efc.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "iMac Pro 27' (MQ2Y2) 2017",
			Category: CategoryIMacs,
			Price:    180999,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/3/8/38cffa1cf15c4bfe03a602c8d742ae71.jpg",
		},

		{
			ID:       uuid.New().String(),
			Company:  "Meizu",
			Title:    "POP2 True Wireless Bluetooth Sports Earphones",
			Category: CategoryEarphones,
			Price:    2199,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/1/2/122d3337163d158e956367b4ce2f1b84.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "EarPods with Remote and Mic (ZKMNHF2ZMA)",
			Category: CategoryEarphones,
			Price:    1199,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/a/7/a78cd2b1f0203bfc9a46da10aa9d2725.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Apple",
			Title:    "AirPods 2019 (2 поколения) with Charging Case",
			Category: CategoryEarphones,
			Price:    6599,
			Sale:     9,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/0/1/015d5f1d2dedae0c489753d34bec1296.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Beats",
			Title:    "Studio 3 Wireless Over-Ear (The Skyline Collection - Shadow Grey) MQUF2XM/A",
			Category: CategoryEarphones,
			Price:    10499,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/3/4/34e3589f0e808f08d23ed7a791775ef8.jpg",
		},
		{
			ID:       uuid.New().String(),
			Company:  "Beats",
			Title:    "Studio 3 Wireless Decade Collection (Black-Red)",
			Category: CategoryEarphones,
			Price:    10499,
			Image:    "https://i.citrus.ua/imgcache/size_800/uploads/shop/3/b/3b08112351a709e9e05a1cc733a4d59b.jpg",
		},
	}

	ItemsCategories = func() (arr []string) {
		mp := make(map[string]struct{})
		for _, i := range ItemsSource {
			mp[i.Category] = struct{}{}
		}
		for k, _ := range mp {
			arr = append(arr, k)
		}
		return
	}()
)
