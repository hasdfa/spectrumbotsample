package tempData

var (
	Sales = func() (arr []*Item) {
		for _, itm := range ItemsSource {
			if itm.Sale > 0 {
				arr = append(arr, itm)
			}
		}
		return
	}()

	SalesCategories = func() (arr []string) {
		mp := make(map[string]struct{})
		for _, i := range Sales {
			mp[i.Category] = struct{}{}
		}
		for k, _ := range mp {
			arr = append(arr, k)
		}
		return
	}()
)
