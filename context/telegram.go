package context

import (
	"fmt"
	"github.com/Syfaro/telegram-bot-api"
	"github.com/google/uuid"
	"spectrumBotSample/tempData"
	"spectrumBotSample/utils/callbackTypes"
	"spectrumBotSample/utils/types"
	"strings"
)

type Telegram struct {
	Api    *tgbotapi.BotAPI
	Update tgbotapi.Update
	Type   callbackTypes.Type

	State *State
}

func (ctx *Telegram) MustSend(chat tgbotapi.Chattable) (msg tgbotapi.Message) {
	var err error

	if msg, err = ctx.Send(chat); err != nil {
		ctx.Error(err)
	}
	return
}

func (ctx *Telegram) Send(chat tgbotapi.Chattable) (msg tgbotapi.Message, err error) {
	defer func() {
		if err == nil {
			fmt.Println(ctx.CmdWithoutAdd(), "!=", ctx.State.ParentCmd())
			if ctx.CmdWithoutAdd() != ctx.State.ParentCmd() {
				fmt.Println("ENTER")
				ctx.State.Push(parentMessageKey, ctx.CmdWithoutAdd())
				ctx.State.LatestMessage().Push(&types.ChattableStackItem{
					Chat:   chat,
					Parent: ctx.CmdWithoutAdd(),
				})
			}

			tempData.StaticSaver.Store(uuid.New().String(), []int64{
				msg.Chat.ID, int64(msg.MessageID),
			})
		}
	}()
	return ctx.Api.Send(chat)
}

func (ctx *Telegram) DeleteMessage() {
	_, _ = ctx.Api.Send(tgbotapi.NewDeleteMessage(ctx.ChatId(), ctx.MessageId()))
}

func (ctx *Telegram) DeleteAll() {
	tempData.StaticSaver.Range(func(_, value interface{}) bool {
		ids := value.([]int64)
		if ctx.ChatId() == ids[0] {
			fmt.Println(ctx.Api.Send(tgbotapi.NewDeleteMessage(ids[0], int(ids[1]))))
		}
		return true
	})
}

func (ctx *Telegram) Error(e error) {
	ctx.ErrorStr(e.Error())
}

func (ctx *Telegram) ErrorStr(e string) {
	fmt.Println("Error:", e)
	if ctx.Update.Message != nil {
		ctx.DeleteMessage()
	}
}

func (ctx *Telegram) Cmd() string {
	if ctx.Update.Message != nil {
		return ctx.Update.Message.Command()
	} else if ctx.Update.CallbackQuery != nil {
		return ctx.Update.CallbackQuery.Data
	}
	return ""
}

func (ctx *Telegram) CmdWithoutAdd() string {
	return strings.Split(ctx.Cmd(), "@")[0]
}

func (ctx *Telegram) CmdAddParam() string {
	return strings.TrimPrefix(ctx.Cmd(), ctx.CmdWithoutAdd()+"@")
}

func (ctx *Telegram) Text() string {
	if ctx.Update.Message != nil {
		return ctx.Update.Message.Text
	} else if ctx.Update.CallbackQuery != nil {
		return ctx.Update.CallbackQuery.Message.Text
	}
	return ""
}

func (ctx *Telegram) ChatId() (i int64) {
	defer func() {
		if i > 0 {
			ctx.State.Push("chatID", i)
		}
	}()
	if ctx.Update.Message != nil {
		return ctx.Update.Message.Chat.ID
	} else if ctx.Update.CallbackQuery != nil {
		return ctx.Update.CallbackQuery.Message.Chat.ID
	} else {
		return ctx.State.GetValue("chatID").(int64)
	}

	return -1
}

func (ctx *Telegram) MessageId() (i int) {
	if ctx.Update.Message != nil {
		return ctx.Update.Message.MessageID
	} else if ctx.Update.CallbackQuery != nil {
		return ctx.Update.CallbackQuery.Message.MessageID
	}

	return -1
}
