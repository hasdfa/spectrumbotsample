package context

import (
	"github.com/Syfaro/telegram-bot-api"
	"spectrumBotSample/tempData"
	"spectrumBotSample/utils/types"
	"sync"
)

const (
	lastMessageKey   = "last_message^(8jk23kj1h(&(*$^%ID_not_REPEAT\\\\\\"
	parentMessageKey = "parent_message_cmd^(8jk23kj1h(&(*$^%ID_not_REPEAT\\\\\\"
)

var (
	sharedState = &sync.Map{}
)

type State struct {
	Id int
	mp map[string]interface{}
}

func RecoverState(up tgbotapi.Update) *State {
	stateId := up.UpdateID
	if up.Message != nil {
		stateId = int(up.Message.Chat.ID)
	} else if up.CallbackQuery != nil {
		stateId = int(up.CallbackQuery.Message.Chat.ID)
	}

	if state, ok := sharedState.Load(stateId); ok {
		return state.(*State)
	}
	state := &State{
		Id: stateId,
		mp: make(map[string]interface{}),
	}

	sharedState.Store(stateId, state)
	return state
}

func (s *State) Push(key string, value interface{}) {
	s.mp[key] = value
}

func (s *State) GetValue(key string) interface{} {
	if v, ok := s.mp[key]; ok {
		return v
	}
	return nil
}

func (s *State) GetString(key string) string {
	if v, ok := s.mp[key]; ok {
		return v.(string)
	}
	return ""
}

func (s *State) GetInt(key string) int {
	if v, ok := s.mp[key]; ok {
		return v.(int)
	}
	return 0
}

func (s *State) PushCart(cart tempData.Cart) {
	s.Push("cart", cart)
}

func (s *State) PushItems(items []*tempData.Item) {
	s.Push("items", items)
}

func (s *State) GetItems() []*tempData.Item {
	if v, ok := s.mp["items"]; ok {
		return v.([]*tempData.Item)
	}
	return make([]*tempData.Item, 0)
}

func (s *State) GetCart() tempData.Cart {
	if v, ok := s.mp["cart"]; ok {
		return v.(tempData.Cart)
	}
	return make(tempData.Cart)
}

func (s *State) LatestMessage() *types.ChattableStack {
	if v, ok := s.mp[lastMessageKey]; ok {
		return v.(*types.ChattableStack)
	}
	stack := &types.ChattableStack{}
	s.mp[lastMessageKey] = stack
	return stack
}

func (s *State) ParentCmd() string {
	if v, ok := s.mp[parentMessageKey]; ok {
		return v.(string)
	}
	return ""
}

func (s *State) Remove(key string) {
	delete(s.mp, key)
}
