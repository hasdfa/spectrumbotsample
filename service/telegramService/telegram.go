package telegramService

import (
	"encoding/json"
	"fmt"
	"github.com/Syfaro/telegram-bot-api"
	"github.com/ivpusic/grpool"
	"github.com/kataras/iris"
	irisContext "github.com/kataras/iris/context"
	"io/ioutil"
	"log"
	"spectrumBotSample/context"
	"spectrumBotSample/tempData"
	"spectrumBotSample/utils"
	"strconv"
	"sync"
)

const (
	workerCount = 1000
)

type IService interface {
	StartUpdate(token string) error
	StartWebhook(token string) error
	Wait()
	Stop()

	DeleteAll()
}

type telegramService struct {
	pool   *grpool.Pool
	api    *tgbotapi.BotAPI
	wg     sync.WaitGroup
	router *Router

	server *iris.Application
}

func New(router *Router) IService {
	return &telegramService{
		router: router,
		wg:     sync.WaitGroup{},
		pool:   grpool.NewPool(workerCount, workerCount),
	}
}

func (s *telegramService) StartWebhook(token string) (err error) {
	s.wg.Add(1)
	s.server = iris.Default()
	s.api, err = tgbotapi.NewBotAPI(token)
	if err != nil {
		return
	}
	fmt.Println("Initialized api")
	fmt.Println("CERT: ", *utils.CertFile, "\n", "KEY: ", *utils.KeyFile)
	s.api.Debug = false

	resp, err := s.api.RemoveWebhook()
	fmt.Println(resp, err)
	if err != nil {
		panic(err)
	}

	path := "/telegram/webhook/" + s.api.Token
	s.startHttp(path)
	config := tgbotapi.NewWebhook("https://tgbot.avesoft.com.ua" + path)
	fmt.Println("Initialized config")

	resp, err = s.api.SetWebhook(config)
	fmt.Println(resp, err)
	if err != nil {
		s.api = nil
		s.StartUpdate(token)
		return
	}

	info, err := s.api.GetWebhookInfo()
	if err != nil {
		s.api = nil
		s.StartUpdate(token)
		return
	}
	if info.LastErrorDate != 0 {
		log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
	}
	return
}

func (s *telegramService) startHttp(path string) {
	s.server.Post(path, func(ctx irisContext.Context) {
		bytes, _ := ioutil.ReadAll(ctx.Request().Body)
		ctx.Request().Body.Close()

		var update tgbotapi.Update
		json.Unmarshal(bytes, &update)

		s.pool.JobQueue <- s.handle(update)
	})
	go s.server.Run(
		iris.Addr(":80"),
	)
	go s.server.Run(
		iris.TLS(":443", *utils.CertFile, *utils.KeyFile),
	)
}

func (s *telegramService) StartUpdate(token string) (err error) {
	s.wg.Add(1)

	s.api, err = tgbotapi.NewBotAPI(token)
	if err != nil {
		return
	}
	fmt.Println("Initialized api")
	s.api.Debug = true

	resp, err := s.api.RemoveWebhook()
	fmt.Println(resp, err)
	if err != nil {
		panic(err)
	}

	config := tgbotapi.NewUpdate(0)
	config.Timeout = 10
	fmt.Println("Initialized config")

	cn, err := s.api.GetUpdatesChan(config)
	if err == nil {
		fmt.Println("Started listening")
		go s.updater(cn)
	}
	return
}

func (s *telegramService) updater(cn tgbotapi.UpdatesChannel) {
	for {
		select {
		case up := <-cn:
			s.pool.JobQueue <- s.handle(up)
		}
	}
}

func (s *telegramService) handle(up tgbotapi.Update) func() {
	return func() {
		s.router.handle(&context.Telegram{
			Api:    s.api,
			Update: up,
			State:  context.RecoverState(up),
		})
	}
}

func (s *telegramService) Wait() {
	s.wg.Wait()
}

func (s *telegramService) Stop() {
	defer s.wg.Done()
	s.api.StopReceivingUpdates()
}

func (s *telegramService) DeleteAll() {
	chats := make(map[int64]struct{})
	tempData.StaticSaver.Range(func(_, value interface{}) bool {
		ids := value.([]int64)

		_, err := s.api.Send(tgbotapi.NewDeleteMessage(ids[0], int(ids[1])))
		fmt.Println(err)
		chats[ids[0]] = struct{}{}
		return true
	})

	for c := range chats {
		msg := tgbotapi.NewMessage(c, "Бот был перезагружен, нажмите ✅ Перезапустить для начала")
		msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("✅ Перезапустить", "start"),
			),
		)
		res, err := s.api.Send(msg)
		if err == nil {
			tgbotapi.NewEditMessageReplyMarkup(c, res.MessageID, tgbotapi.NewInlineKeyboardMarkup(
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData("✅ Перезапустить", "start@"+strconv.Itoa(res.MessageID)),
				),
			))
		}
	}
}
