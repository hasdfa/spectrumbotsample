package telegramService

import (
	"fmt"
	"spectrumBotSample/context"
	"spectrumBotSample/utils/callbackTypes"
	"strings"
)

type (
	Callback func(*context.Telegram)

	CallbackHandler struct {
		Cmd     string
		Type    callbackTypes.Type
		Handler Callback

		Children []CallbackHandler
	}

	Router struct {
		router    map[string]CallbackHandler
		otherwise Callback
	}
)

func NewRouter(arr []CallbackHandler, otherwise Callback) *Router {
	return (&Router{
		router:    make(map[string]CallbackHandler),
		otherwise: otherwise,
	}).init(arr)
}

func (r *Router) init(arr []CallbackHandler) *Router {
	for _, elem := range arr {
		r.router[r.keyOf(elem.Cmd, elem.Type)] = elem
		if elem.Children != nil && len(elem.Children) > 0 {
			r.init(elem.Children)
		}
	}
	return r
}

func (r *Router) keyOf(cmd string, tp callbackTypes.Type) string {
	cmd = strings.Split(cmd, "@")[0]
	return cmd + ":" + tp.String()
}

func (r *Router) handle(ctx *context.Telegram) {
	defer func() {
		if err := recover(); err != nil {
			r.otherwise(ctx)
			fmt.Println(err)
		}
	}()

	ctx.Type = callbackTypes.Undefined

	if ctx.Update.CallbackQuery != nil {
		ctx.Type = callbackTypes.CallbackQuery
	} else if ctx.Update.InlineQuery != nil {
		ctx.Type = callbackTypes.InlineQuery
	} else if ctx.Update.ShippingQuery != nil {
		ctx.Type = callbackTypes.ShippingQuery
	} else if ctx.Update.PreCheckoutQuery != nil {
		ctx.Type = callbackTypes.PreCheckoutQuery
	} else if ctx.Update.Message != nil {
		if ctx.Update.Message.IsCommand() {
			ctx.Type = callbackTypes.Cmd
		} else if ctx.Update.Message.Audio != nil {
			ctx.Type = callbackTypes.Audio
		} else if ctx.Update.Message.Document != nil {
			ctx.Type = callbackTypes.Document
		} else if ctx.Update.Message.Animation != nil {
			ctx.Type = callbackTypes.Animation
		} else if ctx.Update.Message.Game != nil {
			ctx.Type = callbackTypes.Game
		} else if ctx.Update.Message.Photo != nil {
			ctx.Type = callbackTypes.Photo
		} else if ctx.Update.Message.Sticker != nil {
			ctx.Type = callbackTypes.Sticker
		} else if ctx.Update.Message.Video != nil {
			ctx.Type = callbackTypes.Video
		} else if ctx.Update.Message.VideoNote != nil {
			ctx.Type = callbackTypes.VideoNote
		} else if ctx.Update.Message.Voice != nil {
			ctx.Type = callbackTypes.Voice
		} else if ctx.Update.Message.Contact != nil {
			ctx.Type = callbackTypes.Contact
		} else if ctx.Update.Message.Location != nil {
			ctx.Type = callbackTypes.Location
		} else if ctx.Update.Message.Venue != nil {
			ctx.Type = callbackTypes.Venue
		} else if ctx.Update.Message.Invoice != nil {
			ctx.Type = callbackTypes.Invoice
		} else if ctx.Update.Message.SuccessfulPayment != nil {
			ctx.Type = callbackTypes.SuccessfulPayment
		} else if ctx.Update.Message.PassportData != nil {
			ctx.Type = callbackTypes.PassportData
		}
	}

	fmt.Printf("Route %s -> %s\n", ctx.Cmd(), ctx.Type.String())
	if callback, ok := r.router[r.keyOf(ctx.Cmd(), ctx.Type)]; ok {
		callback.Handler(ctx)
	} else {
		r.otherwise(ctx)
	}
}
