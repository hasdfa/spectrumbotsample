package types

import "github.com/Syfaro/telegram-bot-api"

type (
	ChattableStack struct {
		src []*ChattableStackItem
	}

	ChattableStackItem struct {
		Chat   tgbotapi.Chattable
		Parent string
	}
)

func NewChattableStack() *ChattableStack {
	return &ChattableStack{
		src: make([]*ChattableStackItem, 0, 0),
	}
}

func (s *ChattableStack) Push(v *ChattableStackItem) {
	s.src = append(s.src, v)
}

func (s *ChattableStack) Pop() *ChattableStackItem {
	l := len(s.src)
	if l == 0 {
		return nil
	}
	defer func() { s.src = s.src[:l-1] }()
	return s.src[l-1]
}
