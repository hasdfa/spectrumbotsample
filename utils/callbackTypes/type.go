package callbackTypes

type (
	Type int
)

const (
	Cmd = iota
	Audio
	Document
	Animation
	Game
	Photo
	Sticker
	Video
	VideoNote
	Voice
	Contact
	Location
	Venue
	Invoice
	SuccessfulPayment
	PassportData

	CallbackQuery
	InlineQuery
	ShippingQuery
	PreCheckoutQuery

	Undefined
)

var (
	stringValueOf = map[Type]string{
		Cmd:               "Cmd",
		Audio:             "Audio",
		Document:          "Document",
		Animation:         "Animation",
		Game:              "Game",
		Photo:             "Photo",
		Sticker:           "Sticker",
		Video:             "Video",
		VideoNote:         "VideoNote",
		Voice:             "Voice",
		Contact:           "Contact",
		Location:          "Location",
		Venue:             "Venue",
		Invoice:           "Invoice",
		SuccessfulPayment: "SuccessfulPayment",
		PassportData:      "PassportData",
		CallbackQuery:     "CallbackQuery",
		InlineQuery:       "InlineQuery",
		ShippingQuery:     "ShippingQuery",
		PreCheckoutQuery:  "PreCheckoutQuery",
		Undefined:         "Undefined",
	}
)

func (t Type) String() string {
	return stringValueOf[t]
}

func (t Type) Value() int {
	return int(t)
}
