package utils

import "flag"

var (
	CertFile = flag.String("cert", "./ssl/cert.pem", "Define cer file")
	KeyFile  = flag.String("key", "./ssl/key.pem", "Define key file")
)
