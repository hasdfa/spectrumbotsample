package categoriesHandlers

import (
	"github.com/Syfaro/telegram-bot-api"
	"spectrumBotSample/context"
	"spectrumBotSample/controller/marketHandlers"
	"spectrumBotSample/tempData"
)

func CategoriesHandler(ctx *context.Telegram) {
	defer ctx.DeleteMessage()

	msg := tgbotapi.NewMessage(ctx.ChatId(),
		"🗄 Категории: Выберите из списка")
	buttons := tgbotapi.NewInlineKeyboardMarkup()
	for _, c := range tempData.SalesCategories {
		buttons.InlineKeyboard = append(buttons.InlineKeyboard,
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData(c, "categories:select@"+c),
			),
		)
	}
	buttons.InlineKeyboard = append(buttons.InlineKeyboard,
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("🏠 <- Меню", "start"),
		),
	)
	msg.ReplyMarkup = buttons
	ctx.MustSend(msg)
}

func CategoryView(ctx *context.Telegram) {
	category := ctx.CmdAddParam()
	ctx.State.Push("page", 0)
	ctx.State.PushItems(func() (arr []*tempData.Item) {
		for _, i := range tempData.ItemsSource {
			if i.Category == category {
				arr = append(arr, i)
			}
		}
		return
	}())
	marketHandlers.PushItemsView(ctx)
}
