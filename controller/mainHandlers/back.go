package mainHandlers

import "spectrumBotSample/context"

func BackHandler(ctx *context.Telegram) {
	defer func() {
		if recover() != nil {
			StartHandler(ctx)
		}
	}()

	stack := ctx.State.LatestMessage()
	last := stack.Pop()
	current := stack.Pop()

	for current.Parent != last.Parent {
		current = stack.Pop()
	}

	ctx.Api.Send(current.Chat)
	ctx.DeleteMessage()
}
