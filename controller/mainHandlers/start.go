package mainHandlers

import (
	"github.com/Syfaro/telegram-bot-api"
	"spectrumBotSample/context"
	"strconv"
)

func StartHandler(ctx *context.Telegram) {
	defer ctx.DeleteMessage()
	if ctx.CmdAddParam() != "" {
		id, err := strconv.Atoi(ctx.CmdAddParam())
		if err == nil {
			ctx.Api.Send(tgbotapi.NewDeleteMessage(ctx.ChatId(), id))
		}
	}

	cartInfo := "Пусто"
	cartCount := ctx.State.GetCart().Count()
	if cartCount > 0 {
		cartInfo = strconv.Itoa(cartCount)
	}

	msg := tgbotapi.NewMessage(ctx.ChatId(),
		"Привествуем ваc в 🛍EShop - ТЕСТОВЫЙ магазин от Spectrum Development House")
	buttons := tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("🎁 Акции", "menu:sales"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("🗄 Категории", "menu:categories"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("🛒 Корзина ("+cartInfo+")", "menu:cart"),
		),
	)
	msg.ReplyMarkup = buttons

	ctx.MustSend(msg)
}
