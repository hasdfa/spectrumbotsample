package cartHandlers

import (
	"fmt"
	"github.com/Syfaro/telegram-bot-api"
	"github.com/google/uuid"
	"spectrumBotSample/context"
	"spectrumBotSample/tempData"
	"spectrumBotSample/utils"
	"strconv"
)

func CartViewHandler(ctx *context.Telegram) {
	defer ctx.DeleteMessage()

	cart := ctx.State.GetCart()
	text := "*🛒 Ваша корзина:*\n\n"
	var finalPrice float32
	for _, item := range cart {
		itemPrice := item.Item.FinalPrice() * float32(item.Count)
		text += fmt.Sprintf("*•* %s %s\n			%s × %d -> %.2f\n\n",
			item.Item.Company, item.Item.Title, item.Item.FinalPriceString(), item.Count, itemPrice)
		finalPrice += itemPrice
	}
	text += fmt.Sprintf("\n_Итого: %.2f_", finalPrice)

	msg := tgbotapi.NewMessage(ctx.ChatId(), text)
	msg.ParseMode = "Markdown"
	msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Купить", "cart:buy"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Очистить корзину ❌", "cart:clear"),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("🏠 <- Меню", "start"),
		),
	)

	ctx.MustSend(msg)
}

func CartBuyHandler(ctx *context.Telegram) {
	ctx.DeleteMessage()
	cart := ctx.State.GetCart()
	var prices []tgbotapi.LabeledPrice

	invoiceUUID := uuid.New().String()
	ctx.State.Push("invoice", invoiceUUID)
	amoun := 0

	for _, item := range cart {
		itemPrice := item.Item.FinalPrice() * float32(item.Count)
		prices = append(prices, tgbotapi.LabeledPrice{
			Label:  item.Item.Company + " " + item.Item.Title,
			Amount: int(itemPrice * 100),
		})
		amoun += int(itemPrice * 100)
	}
	fmt.Println(amoun)

	msg := tgbotapi.NewInvoice(
		ctx.ChatId(),
		"Нажмите для продолжения", "Деньги не будут сняты, это тестовая покупка.",
		invoiceUUID, utils.CT_LiqPayTestToken, invoiceUUID, "UAH", &prices,
	)
	msg.NeedName = true
	msg.NeedPhoneNumber = true
	m, err := ctx.Send(msg)
	if err != nil {
		ctx.State.PushCart(make(tempData.Cart))
		msg := tgbotapi.NewMessage(ctx.ChatId(), "*Не удалось совершить покупку:* \n`"+err.Error()+"`\n\n\t\tНажмите _✅ Перезапустить_ для выхода в главное меню")
		msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("✅ Перезапустить", "start"),
			),
		)
		msg.ParseMode = "Markdown"
		res, err := ctx.Send(msg)
		if err == nil {
			tgbotapi.NewEditMessageReplyMarkup(ctx.ChatId(), res.MessageID, tgbotapi.NewInlineKeyboardMarkup(
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData("✅ Перезапустить", "start@"+strconv.Itoa(res.MessageID)),
				),
			))
		}
		return
	}

	backMsg := tgbotapi.NewMessage(ctx.ChatId(), "Меню")
	backMsg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Вернуться в меню 🏠", "start@"+strconv.Itoa(m.MessageID)),
		),
	)
	ctx.MustSend(backMsg)
}
