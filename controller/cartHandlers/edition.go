package cartHandlers

import (
	"spectrumBotSample/context"
	"spectrumBotSample/controller/mainHandlers"
	"spectrumBotSample/controller/marketHandlers"
	"spectrumBotSample/tempData"
)

func HandleAdd(ctx *context.Telegram) {
	id := ctx.CmdAddParam()
	cart := ctx.State.GetCart()
	var ok bool

	if _, ok = cart[id]; !ok {
		items := ctx.State.GetItems()
		for _, i := range items {
			if i.ID == id {
				cart[id] = &tempData.CartItem{
					Item:  i,
					Count: 1,
				}
				break
			}
		}
	} else {
		cart[id].Count += 1
	}
	ctx.State.PushCart(cart)
	marketHandlers.PushItemsView(ctx)
}

func HandleRemove(ctx *context.Telegram) {
	id := ctx.CmdAddParam()
	cart := ctx.State.GetCart()

	delete(cart, id)
	ctx.State.PushCart(cart)
	marketHandlers.PushItemsView(ctx)
}

func HandleClear(ctx *context.Telegram) {
	ctx.DeleteAll()
	ctx.State.PushCart(make(tempData.Cart))
	mainHandlers.StartHandler(ctx)
}
