package cartHandlers

import (
	"spectrumBotSample/context"
	"spectrumBotSample/controller/mainHandlers"
)

func PreCheckoutHandler(ctx *context.Telegram) {
	ctx.DeleteAll()
	mainHandlers.StartHandler(ctx)
}
