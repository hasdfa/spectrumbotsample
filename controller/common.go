package controller

import (
	"fmt"
	"runtime"
	"spectrumBotSample/context"
	"spectrumBotSample/controller/cartHandlers"
	"spectrumBotSample/controller/categoriesHandlers"
	"spectrumBotSample/controller/mainHandlers"
	"spectrumBotSample/controller/marketHandlers"
	"spectrumBotSample/controller/saleHandlers"
	"spectrumBotSample/service/telegramService"
	"spectrumBotSample/utils/callbackTypes"
	"strconv"
)

var (
	DefaultRouter = telegramService.NewRouter(
		[]telegramService.CallbackHandler{
			{
				Cmd:     "start",
				Type:    callbackTypes.Cmd,
				Handler: mainHandlers.StartHandler,
			},
			{
				Cmd:     "start",
				Type:    callbackTypes.CallbackQuery,
				Handler: mainHandlers.StartHandler,
			},
			{
				Type:    callbackTypes.PreCheckoutQuery,
				Handler: cartHandlers.PreCheckoutHandler,
			},

			{
				Cmd:     "menu:sales",
				Type:    callbackTypes.CallbackQuery,
				Handler: saleHandlers.SalesHandler,

				Children: []telegramService.CallbackHandler{
					{
						Cmd:     "sales:select",
						Type:    callbackTypes.CallbackQuery,
						Handler: saleHandlers.CategoryView,
					},
				},
			},
			{
				Cmd:     "menu:categories",
				Type:    callbackTypes.CallbackQuery,
				Handler: categoriesHandlers.CategoriesHandler,

				Children: []telegramService.CallbackHandler{
					{
						Cmd:     "categories:select",
						Type:    callbackTypes.CallbackQuery,
						Handler: categoriesHandlers.CategoryView,
					},
				},
			},
			{
				Cmd:     "menu:cart",
				Type:    callbackTypes.CallbackQuery,
				Handler: cartHandlers.CartViewHandler,

				Children: []telegramService.CallbackHandler{
					{
						Cmd:     "cart:add",
						Type:    callbackTypes.CallbackQuery,
						Handler: cartHandlers.HandleAdd,
					},
					{
						Cmd:     "cart:remove",
						Type:    callbackTypes.CallbackQuery,
						Handler: cartHandlers.HandleRemove,
					},
					{
						Cmd:     "cart:clear",
						Type:    callbackTypes.CallbackQuery,
						Handler: cartHandlers.HandleClear,
					},
					{
						Cmd:     "cart:buy",
						Type:    callbackTypes.CallbackQuery,
						Handler: cartHandlers.CartBuyHandler,
					},
				},
			},
			{
				Cmd:     "market:prev",
				Type:    callbackTypes.CallbackQuery,
				Handler: marketHandlers.PrevItemHandler,
			},
			{
				Cmd:     "market:next",
				Type:    callbackTypes.CallbackQuery,
				Handler: marketHandlers.NextItemHandler,
			},
		},
		OtherwiseHandler,
	)
)

func OtherwiseHandler(ctx *context.Telegram) {
	defer mainHandlers.StartHandler(ctx)
	defer ctx.DeleteMessage()
	defer ctx.DeleteAll()

	_, file, line, _ := runtime.Caller(1)
	fmt.Println("Caller: " + file + ":" + strconv.Itoa(line))

	msg := ctx.State.LatestMessage()
	last := msg.Pop()
	for last != nil {
		last = msg.Pop()
	}
}
