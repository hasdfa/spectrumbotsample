package marketHandlers

import (
	"spectrumBotSample/context"
)

func PrevItemHandler(ctx *context.Telegram) {
	page := ctx.State.GetInt("page") - 1
	ctx.State.Push("page", page)

	PushItemsView(ctx)
}

func NextItemHandler(ctx *context.Telegram) {
	page := ctx.State.GetInt("page") + 1
	ctx.State.Push("page", page)

	PushItemsView(ctx)
}
