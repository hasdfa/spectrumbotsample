package marketHandlers

import (
	"fmt"
	"github.com/Syfaro/telegram-bot-api"
	"spectrumBotSample/context"
	"strconv"
)

func PushItemsView(ctx *context.Telegram) {
	defer ctx.DeleteMessage()
	page := ctx.State.GetInt("page")
	items := ctx.State.GetItems()
	cart := ctx.State.GetCart()
	item := items[page]

	msg := tgbotapi.NewPhotoUpload(ctx.ChatId(), tgbotapi.FileBytes{Bytes: item.ImageBytes()})
	msg.ParseMode = "Markdown"
	if item.Sale > 0 {
		msg.Caption = fmt.Sprintf("%s _%s_ (%s -%d%%) *%s*", item.Company, item.Title, item.ZPrice(), item.Sale, item.PriceWithSale())
	} else {
		msg.Caption = fmt.Sprintf("%s _%s_ *%.2f*", item.Company, item.Title, item.Price)
	}
	buttons := tgbotapi.NewInlineKeyboardMarkup()

	if cartItem, ok := cart[item.ID]; ok {
		buttons.InlineKeyboard = append(buttons.InlineKeyboard, tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Добавить в корзину ("+strconv.Itoa(int(cartItem.Count))+")", "cart:add@"+item.ID),
		))
		buttons.InlineKeyboard = append(buttons.InlineKeyboard, tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Удалить из корзины", "cart:remove@"+item.ID),
		))
	} else {
		buttons.InlineKeyboard = append(buttons.InlineKeyboard, tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Добавить в корзину", "cart:add@"+item.ID),
		))
	}

	if len(items) > 1 {
		if page == 0 {
			buttons.InlineKeyboard = append(buttons.InlineKeyboard,
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData("->", "market:next"),
				),
			)
		} else if page == len(items)-1 {
			buttons.InlineKeyboard = append(buttons.InlineKeyboard,
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData("<-", "market:prev"),
				),
			)
		} else {
			buttons.InlineKeyboard = append(buttons.InlineKeyboard,
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData("<-", "market:prev"),
					tgbotapi.NewInlineKeyboardButtonData("->", "market:next"),
				),
			)
		}
	}

	buttons.InlineKeyboard = append(buttons.InlineKeyboard,
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("🏠 <- Меню", "start"),
		),
	)

	msg.ReplyMarkup = &buttons
	ctx.MustSend(msg)
}
